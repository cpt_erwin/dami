<?php

namespace app\events;

use Yii;
use \yii\web\Cookie;

class UserEvents {

	public static function handleAfterLogin($event)
	{
		$lastLogin = $event->identity->getLastLogin();

		// Set last time user logged in as a cookie for dashboard panel
		if ($event->identity->updateLastLogin()) {
			Yii::$app->getResponse()->getCookies()->add(new Cookie(['name' => 'lastLogin', 'value' => $lastLogin]));
		}
	}
}