<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
	'defaultRoute' => 'article/index',
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
		'assetManager' => [
			'converter' => [
				'class' => 'yii\web\AssetConverter',
				'commands' => [
					'scss' => ['css', 'sass {from} {to} --source-map'],
					'sass' => ['css', 'sass {from} {to} --source-map']
				],
			],
		],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'YErWFUcNeOY8D2m7E3hFghB4RncQHcbt',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		'authClientCollection' => [
			'class' => 'yii\authclient\Collection',
			'clients' => [
				'facebook' => [
					'class' => 'yii\authclient\clients\Facebook',
					'clientId' => '2203880523070907',
					'clientSecret' => '1336748f9faa830b6f45fe3c77407d68',
				],
			],
		],
        'user' => [
            'identityClass' => 'app\models\User',
			'loginUrl' => 'auth/login',
            'enableAutoLogin' => true,
			'on '.\yii\web\User::EVENT_AFTER_LOGIN => ['app\events\UserEvents', 'handleAfterLogin'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
			// Disable index.php
			'showScriptName' => false,
			// Disable r= routes
			'enablePrettyUrl' => true,
			'rules' => array(
				// Specific
				'auth/password/<token>' => 'auth/change-password',

				'article' => 'article/index',
				'article/index' => 'article/index',
				'article/create' => 'article/create',
				'article/view/<id:\d+>' => 'article/view',
				'article/update/<id:\d+>' => 'article/update',
				'article/delete/<id:\d+>' => 'article/delete',
				'article/<slug>' => 'article/slug',

				// Global
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
