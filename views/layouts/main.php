<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
	<?php
	if (!Yii::$app->user->isGuest) {
		NavBar::begin([
			'brandLabel' => Html::img('@web/images/logo.png', ['style' => 'max-width:100px; margin-top: -4px;', 'alt' => Yii::$app->name]),
			'brandUrl' => Yii::$app->homeUrl,
			'options' => [
				'class' => 'navbar-inverse navbar-fixed-top',
			],
		]);

		echo Nav::widget([
			'options' => ['class' => 'navbar-nav navbar-right'],
			'items' => [
				['label' => 'Home', 'url' => ['/article/index']],
				(
					'<li>'
					. Html::beginForm(['/auth/logout'], 'post')
					. Html::submitButton(
						'Sign out',
						['class' => 'btn btn-link logout']
					)
					. Html::endForm()
					. '</li>'
				),
                (
                    '<li>'
                    . '<p class="navbar-text navbar-right">'
					. '<span class="glyphicon glyphicon-user"></span> '
                    . Yii::$app->user->identity->fullName
                    . '</p>'
                    . '</li>'
                )
			],
		]);

		NavBar::end();
	}
	?>

    <div class="container">
		<?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
