<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="cards row">
    <div class="col-sm-12 col-md-4 media-wrapper">
        <div class="media">
            <div class="media-left">
                <span class="media-object glyphicon glyphicon-lock"></span>
            </div>
            <div class="media-body text-left">
                <span>Middle aligned media</span>
                <h4 class="media-heading"><?php echo Yii::$app->user->identity->lastLogin //FIXME: Take from session (remake cookie) ?></h4>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4 media-wrapper">
        <div class="media">
            <div class="media-left">
                <span class="media-object glyphicon glyphicon-file"></span>
            </div>
            <div class="media-body">
                <span>Middle aligned media</span>
                <h4 class="media-heading"><?php echo Yii::$app->user->identity->getArticlesCount() ?></h4>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4 media-wrapper">
        <div class="media">
            <div class="media-left">
                <span class="media-object glyphicon glyphicon-calendar"></span>
            </div>
            <div class="media-body">
                <span>Middle aligned media</span>
                <h4 class="media-heading"><?php echo Yii::$app->user->identity->getLastPublishedArticleDate() ?></h4>
            </div>
        </div>
    </div>
</div>

<hr>

<div class="article article-index">

    <div class="heading">
        <span class="glyphicon glyphicon-file"></span>
        <h4><?= Html::encode($this->title) ?></h4>
		<?= Html::a('<span class="glyphicon glyphicon-plus"></span> Create Article', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'options' => ['class' => 'grid-view card'],
        'columns' => [
            'id',
            'createdAt',
            'title',
			'authorId',

			['class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update} {delete}',
				'buttons' => [
					'view' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['slug', 'slug' => $model->slug], ['title' => Yii::t('yii', 'View'),]);
					}
				],
			],
        ],
    ]); ?>



</div>
