<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Sign in';
?>
<div class="auth-form">
    <?php echo Html::img('@web/images/logo.png', ['class' => 'logo', 'alt' => Yii::$app->name]) ?>

    <div class="login-card">
        <strong class="text-left"><?= Html::encode($this->title) ?></strong>
        <hr>

        <!-- Display success message -->
		<?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success">
				<?= Yii::$app->session->getFlash('success') ?>
            </div>
		<?php endif; ?>

		<?php $form = ActiveForm::begin([
			'id' => 'login-form',
			'layout' => 'horizontal',
			'fieldConfig' => [
				'template' => "<div class=\"col-lg-12\">{input}</div><div class=\"col-lg-12 text-left\">{error}</div>",
			],
		]); ?>

		<?= $form->field($model, 'username')->textInput(['placeholder' => $model->getAttributeLabel('username')])->label(false)->error(false) ?>

		<?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')])->label(false) ?>

        <div class="form-group">
            <div class="col-lg-12">
				<?= Html::submitButton('Sign in', ['class' => 'btn btn-danger btn-block', 'name' => 'login-button']) ?>
            </div>
        </div>

		<?php ActiveForm::end(); ?>

        <div id="auth-links" class="text-right">
            <?= $model->hasErrors() ? Html::a('Did you forget the password?', ['password']) . '<br>' : '' ?>
			<?= Html::a('Manual registration', ['register']) ?>
        </div>

    </div>


	<?= Html::a('Sign in via Facebook', ['auth', 'authclient' => 'facebook'], ['class' => 'btn btn-primary btn-block',]) ?>

</div>
