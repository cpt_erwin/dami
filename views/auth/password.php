<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\PasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Forgotten password';
?>
<div class="auth-form">
	<?php echo Html::img('@web/images/logo.png', ['class' => 'logo', 'alt' => Yii::$app->name]) ?>

	<div class="register-card">
		<strong class="text-left"><?= Html::encode($this->title) ?></strong>
		<hr>

        <!-- Display success message -->
		<?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
				<?= Yii::$app->session->getFlash('success') ?>
            </div>
		<?php elseif (Yii::$app->session->hasFlash('failure')): ?>
            <div class="alert alert-danger">
				<?= Yii::$app->session->getFlash('failure') ?>
            </div>
		<?php endif; ?>

		<?php $form = ActiveForm::begin([
			'id' => 'forgot-form',
			'layout' => 'horizontal',
			'fieldConfig' => [
				'template' => "<div class=\"col-lg-12\">{input}</div><div class=\"col-lg-12 text-left\">{error}</div>",
			],
		]); ?>

		<?= $form->field($model, 'username')->textInput(['placeholder' => $model->getAttributeLabel('username')])->label(false) ?>

		<div class="form-group">
			<div class="col-lg-12">
				<?= Html::submitButton('Recover password', ['class' => 'btn btn-danger btn-block', 'name' => 'register-button']) ?>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>

	<?= Html::a('<span class="glyphicon glyphicon-arrow-left"></span> Back to login', ['login'], ['class' => 'btn btn-danger btn-block',]) ?>

</div>
