<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\PasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Change password';
?>
<div class="auth-form">
	<?php echo Html::img('@web/images/logo.png', ['class' => 'logo', 'alt' => Yii::$app->name]) ?>

	<div class="register-card">
		<strong class="text-left"><?= Html::encode($this->title) ?></strong>
		<hr>

		<?php $form = ActiveForm::begin([
			'id' => 'forgot-form',
			'layout' => 'horizontal',
			'fieldConfig' => [
				'template' => "<div class=\"col-lg-12\">{input}</div><div class=\"col-lg-12 text-left\">{error}</div>",
			],
		]); ?>

		<?= $form->field($model, 'username')->textInput(['disabled' => true])->label(false) ?>
		<?= $form->field($model, 'password')->input('password', ['placeholder' => 'new ' . $model->getAttributeLabel('password')])->label(false) ?>
		<?= $form->field($model, 'passwordRepeat')->input('password', ['placeholder' => 'new ' . $model->getAttributeLabel('passwordRepeat')])->label(false) ?>

		<div class="form-group">
			<div class="col-lg-12">
				<?= Html::submitButton('Change password', ['class' => 'btn btn-danger btn-block', 'name' => 'register-button']) ?>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>

	<?= Html::a('<span class="glyphicon glyphicon-arrow-left"></span> Back to login', ['login'], ['class' => 'btn btn-danger btn-block',]) ?>

</div>
