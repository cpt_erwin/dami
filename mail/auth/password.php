<?php

/* @var $token string */

use yii\helpers\Html;
use yii\helpers\Url;

?>
Hello there,
<br/>
<br/>
....
<br/>
If you want to change your password, click on the link bellow
<br/>
<br/>
<?php echo HTML::encode(Url::to(['auth/change-password', 'token' => $token], true)) ?>