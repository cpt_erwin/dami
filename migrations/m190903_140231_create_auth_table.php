<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auth}}`.
 */
class m190903_140231_create_auth_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%auth}}', [
			'id' => $this->primaryKey(),
			'userId' => $this->integer()->notNull(),
			'source' => $this->string()->notNull(),
			'sourceId' => $this->string()->notNull(),
		]);

		// add foreign key for table `{{%user}}`
		$this->addForeignKey(
			'{{%fk-auth-userId}}',
			'{{%auth}}',
			'userId',
			'{{%user}}',
			'id',
			'CASCADE',
			'CASCADE'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		// drops foreign key for table `{{%user}}`
		$this->dropForeignKey(
			'{{%fk-auth-userId}}',
			'{{%auth}}'
		);

		$this->dropTable('{{%auth}}');
	}
}
