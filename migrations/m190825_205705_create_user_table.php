<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m190825_205705_create_user_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%user}}', [
			'id' => $this->primaryKey(),
			'fullName' => $this->string()->notNull(),
			'email' => $this->string()->notNull(), // email = username
			'password' => $this->char(60)->notNull(), // Yii::$app->getSecurity()->generatePasswordHash($password); uses 'crypt' function which output is always 60 ASCII characters
			'passwordResetToken' => $this->char(32)->notNull(), // Yii::$app->getSecurity()->generateRandomString(); has default length 32
			'passwordResetExpire' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
			'authKey' => $this->char(32)->notNull(),
			'accessToken' => $this->char(32)->notNull(),
			'lastLogin' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),

			// timestamps
			'createdAt' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%user}}');
	}
}
