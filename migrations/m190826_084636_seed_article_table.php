<?php

use yii\db\Migration;

/**
 * Handles the seeding of table `{{%article}}`.
 */
class m190826_084636_seed_article_table extends Migration
{
	/**
	 * {@inheritdoc}
	 * @throws \yii\base\Exception
	 * TODO: Add seeds to separated folder and create command 'yii seed'
	 */
	public function safeUp()
	{
		$transaction = $this->db->beginTransaction();
		try {
			$this->batchInsert('{{%article}}', ['authorId', 'title', 'slug', 'content'], [
				[1, 'Test', 'test', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin in tellus sit amet nibh dignissim sagittis. Praesent dapibus. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Aenean vel massa quis mauris vehicula lacinia. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer vulputate sem a nibh rutrum consequat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Sed ac dolor sit amet purus malesuada congue. Phasellus et lorem id felis nonummy placerat. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Integer lacinia.'],
				[2, 'Hello World', 'hello-world', 'Proin mattis lacinia justo. Aliquam erat volutpat. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Integer imperdiet lectus quis justo. Praesent dapibus. Quisque tincidunt scelerisque libero. Maecenas aliquet accumsan leo. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam sed tellus id magna elementum tincidunt. Phasellus faucibus molestie nisl. Quisque porta. Nullam rhoncus aliquam metus. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus. Nam sed tellus id magna elementum tincidunt. Nunc tincidunt ante vitae massa.'],
				[2, 'How I started with Yii', 'how-i-started-with-yii', 'Morbi scelerisque luctus velit. Fusce consectetuer risus a nunc. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Maecenas libero. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Aenean vel massa quis mauris vehicula lacinia. Aliquam erat volutpat. Nulla quis diam. Vivamus ac leo pretium faucibus. Nulla pulvinar eleifend sem. Nunc dapibus tortor vel mi dapibus sollicitudin. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Mauris tincidunt sem sed arcu.'],
			]);
			$transaction->commit();
		} catch (\yii\base\Exception $exception) {
			throw new \yii\base\Exception("generatePasswordHash() or generatePasswordHash() function has failed while seeding!");
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->truncateTable('{{%article}}');
	}
}
