<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%article}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m190825_222832_create_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'authorId' => $this->integer()->notNull(),
			'title' => $this->string(150)->notNull(),
			'slug' => $this->string()->unique()->notNull(), // this will be URL for view of an article instance
			'content' => $this->text()->notNull(),

			// timestamps
			'createdAt' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
			'updatedAt' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        // creates index for column `authorId`
        $this->createIndex(
            '{{%idx-article-authorId}}',
            '{{%article}}',
            'authorId'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-article-authorId}}',
            '{{%article}}',
            'authorId',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-article-authorId}}',
            '{{%article}}'
        );

        // drops index for column `authorId`
        $this->dropIndex(
            '{{%idx-article-authorId}}',
            '{{%article}}'
        );

        $this->dropTable('{{%article}}');
    }
}
