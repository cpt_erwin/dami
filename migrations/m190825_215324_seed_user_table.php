<?php

use yii\db\Migration;

/**
 * Handles the seeding of table `{{%user}}`.
 */
class m190825_215324_seed_user_table extends Migration
{
	/**
	 * {@inheritdoc}
	 * @throws \yii\base\Exception
	 * TODO: Add seeds to separated folder and create command 'yii seed'
	 */
    public function safeUp()
    {
		$transaction = $this->db->beginTransaction();
    	try {
			$this->batchInsert('{{%user}}', ['fullName', 'email', 'password', 'passwordResetToken', 'authKey', 'accessToken'], [
				['John Doe', 'john.doe@example.com', Yii::$app->getSecurity()->generatePasswordHash('test'), Yii::$app->getSecurity()->generateRandomString(), Yii::$app->getSecurity()->generateRandomString(), Yii::$app->getSecurity()->generateRandomString()],
				['Michal Tuček', 'michaltk1@gmail.com', Yii::$app->getSecurity()->generatePasswordHash('test'), Yii::$app->getSecurity()->generateRandomString(), Yii::$app->getSecurity()->generateRandomString(), Yii::$app->getSecurity()->generateRandomString()],
			]);
			$transaction->commit();
		} catch ( \yii\base\Exception $exception) {
    		throw new \yii\base\Exception("generatePasswordHash() or generatePasswordHash() function has failed while seeding!");
		}
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->truncateTable('{{%user}}');
    }
}
