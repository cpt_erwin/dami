<?php

namespace app\controllers;

use app\components\AuthHandler;
use app\models\ChangePasswordForm;
use app\models\PasswordForm;
use app\models\RegistrationForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;

class AuthController extends Controller
{
	public $layout = 'auth';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'auth' => [
				'class' => 'yii\authclient\AuthAction',
				'successCallback' => [$this, 'onAuthSuccess'],
			],
		];
	}

	/**
	 * Handles login via third-party services.
	 *
	 * @param $client
	 */
	public function onAuthSuccess($client)
	{
		(new AuthHandler($client))->handle();
	}

	/**
	 * Register action.
	 *
	 * @return Response|string
	 * @throws Exception
	 */
	public function actionRegister()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new RegistrationForm();
		if ($model->load(Yii::$app->request->post()) && $model->register()) {
			Yii::$app->getSession()->setFlash('success', Yii::t('app',Html::encode('You\'ve been successfully registered! Now you can login!')));
			$this->redirect(['auth/login']);
		}

		$model->password = '';
		$model->passwordRepeat = '';
		return $this->render('register', [
			'model' => $model,
		]);
	}

	/**
 * Login action.
 *
 * @return Response|string
 */
	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		}

		$model->password = '';
		return $this->render('login', [
			'model' => $model,
		]);
	}

	/**
	 * Forgot password action.
	 *
	 * @return string|Response
	 * @throws Exception
	 */
	public function actionPassword()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new PasswordForm();
		if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
			Yii::$app->getSession()->setFlash('success', 'Check your inbox <strong>' . $model->username . '</strong>. There will be password reset link.');
		}

		return $this->render('password', [
			'model' => $model,
		]);
	}

	/**
	 * Forgot action.
	 *
	 * @param $token string
	 * @return Response|string
	 */
	public function actionChangePassword($token)
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$user = User::findByPasswordResetToken($token);

		$model = new ChangePasswordForm();
		$model->username = $user->email;
		if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
			Yii::$app->getSession()->setFlash('success', Yii::t('app',Html::encode('Your password has been successfully changed! You can now login with your new password!')));
			$this->redirect(['auth/login']);
		}

		// If user goes to link and change password after minute or later, let him do that
		if(!$user->canChangePassword()) {
			Yii::$app->getSession()->setFlash('failure', Yii::t('app',Html::encode('Password change token expired!')));
			$this->redirect(['auth/password']);
		}

		return $this->render('change-password', [
			'model' => $model,
		]);
	}

	/**
	 * Logout action.
	 *
	 * @return Response
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

}
