<?php


namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * RegistrationForm is the model behind the registration form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegistrationForm extends Model
{
	public $fullName;
	public $email;
	public $password;
	public $passwordRepeat;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			// all fields are required
			[['fullName', 'email', 'password', 'passwordRepeat'], 'required'],
			// email must be email and unique - check User table records
			['email', 'email'],
			['email', 'unique',
				'targetClass' => 'app\models\User',
				'targetAttribute' => 'email',
				'message' => 'This Email is already in use'],
			// 6 - 20 characters + must be same as passwordRepeat
			['password', 'string', 'min' => 6, 'max' => 20, 'message' => 'Password must have more than 6 and less then 20 characters!'],
			['passwordRepeat', 'compare', 'compareAttribute' => 'password']
		];
	}

	/**
	 * Register a user using the provided information.
	 * @return bool whether the user is logged in successfully
	 */
	public function register()
	{
		if ($this->validate()) {
			$user = new User([
				'fullName' => $this->fullName,
				'email' => $this->email,
				'password' => Yii::$app->getSecurity()->generatePasswordHash($this->password),
				'authKey' => Yii::$app->getSecurity()->generateRandomString(),
				'accessToken' => Yii::$app->getSecurity()->generateRandomString()
			]);

			$trans = Yii::$app->db->beginTransaction();
			try {
				$user->save();
				$trans->commit();
				return true;
			} catch (Exception $e) {
				$trans->rollback();
				throw $e;
			}
		}
		return false;
	}
}