<?php

namespace app\models;

use Yii;
use yii\base\InvalidArgumentException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $fullName
 * @property string $email
 * @property string $password
 * @property string $passwordResetToken
 * @property string passwordResetExpire
 * @property string $authKey
 * @property string $accessToken
 * @property string $lastLogin
 * @property string $createdAt
 *
 * @property Article[] $articles
 * @property Auth[] $auths
 */
class User extends ActiveRecord implements IdentityInterface
{

	const REMEMBER_ME_DURATION = 2592000; // 3600*24*30

	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'user';
	}


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
		return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		return static::findOne(['accessToken' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
		return self::findByEmail($username);
    }

	/**
	 * Finds user by email
	 *
	 * @param string $email
	 * @return static|null
	 */
	public static function findByEmail($email)
	{
		return static::findOne(['email' => $email]);
	}

	public static function findByPasswordResetToken($token)
	{
		return static::findOne(['passwordResetToken' => $token]);
	}

	/**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

	/**
	 * {@inheritdoc}
	 */
	public function getLastLogin()
	{
		return $this->lastLogin;
	}

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return bool if password provided is valid for current user
	 * @throws InvalidArgumentException
	 */
    public function validatePassword($password)
    {
    	try {
			return Yii::$app->getSecurity()->validatePassword($password, $this->password);
		} catch (InvalidArgumentException $exception) {
    		throw new InvalidArgumentException("Invalid input provided for generatePasswordHash()!");
		}
    }

    public function setPassword($password) {
    	$this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
	}

	/**
	 * Check if password reset token is not expired
	 *
	 * @return bool
	 */
	public function canChangePassword() {
		return strtotime($this->passwordResetExpire) > time();
	}

	/**
	 * Updates last login of user
	 *
	 * @return bool
	 */
    public function updateLastLogin() {
    	$this->lastLogin = date('Y-m-d H:i:s');
		return $this->save(false, ['lastLogin']);
	}

	/**
	 * Sets time for valid password reset.
	 */
	public function setPasswordResetExpire() {
		$this->passwordResetExpire =  date('Y-m-d H:i:s', strtotime('+1 minute', time()));
	}

	/**
	 * Generates password reset token.
	 */
	public function generatePasswordResetToken() {
		$this->passwordResetToken = Yii::$app->getSecurity()->generateRandomString();
	}

	/**
	 * Generates auth key.
	 */
	public function generateAuthKey() {
		$this->authKey = Yii::$app->getSecurity()->generateRandomString();
	}

	/**
	 * Generates access token.
	 */
	public function generateAccessToken() {
		$this->accessToken = Yii::$app->getSecurity()->generateRandomString();
	}

	/**
	 * @return ActiveQuery
	 */
	public function getArticles()
	{
		return $this->hasMany(Article::className(), ['authorId' => 'id']);
	}

	/**
	 * @return int
	 */
	public function getArticlesCount()
	{
		return $this->getArticles()->count();
	}

	/**
	 * @return string
	 */
	public function getLastPublishedArticleDate()
	{
		$article = $this->getArticles()->orderBy('createdAt')->one();
		if ($article instanceof Article) { return $article->createdAt; }
		return 'XX.XX.XXXX XX:XX';
	}

	/**
	 * @return ActiveQuery
	 */
	public function getAuths()
	{
		return $this->hasMany(Auth::className(), ['userId' => 'id']);
	}
}
