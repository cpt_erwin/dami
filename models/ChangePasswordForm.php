<?php


namespace app\models;

use yii\base\Model;

/**
 * ChangePasswordForm is the model behind the change-password form.
 */
class ChangePasswordForm extends Model
{
	public $username;
	public $password;
	public $passwordRepeat;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['password', 'passwordRepeat'] , 'required'],
			['password', 'string', 'min' => 6, 'max' => 20, 'message' => 'Password must have more than 6 and less then 20 characters!'],
			['passwordRepeat', 'compare', 'compareAttribute' => 'password']
		];
	}

	/**
	 * @return bool
	 */
	public function changePassword() {
		if ($this->validate()) {
			$user = User::findByUsername($this->username);
			$user->setPassword($this->password);
			if ($user->validate()) {
				return $user->save();
			}
		}
		return false;
	}

}