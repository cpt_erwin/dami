<?php


namespace app\models;

use Yii;
use yii\base\Model;

/**
 * RegistrationForm is the model behind the registration form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class PasswordForm extends Model
{
	public $username;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			['username', 'required'],
			['username', 'exist', 'targetClass' => User::className(), 'targetAttribute' => ['username' => 'email'], 'message' => Yii::t('yii', 'Username does not exist')],
		];
	}

	/**
	 * @return bool
	 * @throws \yii\base\Exception
	 */
	public function resetPassword() {
		if ($this->validate()) {
			$user = User::findByUsername($this->username);
			$user->generatePasswordResetToken();
			$user->setPasswordResetExpire();

			if ($user->validate()) {
				if (!$user->save()) { throw new \yii\base\Exception('Something went wrong!'); }
				return Yii::$app->mailer->compose('auth/password', ['token' => $user->passwordResetToken])
					->setFrom('system@testproject.damidev.com')
					->setTo($user->email)
					->setSubject('Reset password request')
					->send();
			}
		}
		return false;
	}

}